﻿
namespace Pharmacy
{
    partial class Profile
    {
        /// <summay> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label_fname = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label_lname = new System.Windows.Forms.Label();
            this.label_phone = new System.Windows.Forms.Label();
            this.label_email = new System.Windows.Forms.Label();
            this.label_salary = new System.Windows.Forms.Label();
            this.label_user = new System.Windows.Forms.Label();
            this.label_pass = new System.Windows.Forms.Label();
            this.richTextBox_address = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.8855F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85.1145F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1014, 524);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Script MT Bold", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Info;
            this.label1.Location = new System.Drawing.Point(359, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(295, 72);
            this.label1.TabIndex = 1;
            this.label1.Text = "Credentials";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label_fname, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label5, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label6, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.label7, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.label8, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.label9, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.label10, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.label_lname, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label_phone, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.label_email, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.label_salary, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.label_user, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.label_pass, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.richTextBox_address, 2, 4);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 81);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1008, 440);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(214, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 33);
            this.label2.TabIndex = 0;
            this.label2.Text = "First Name";
            // 
            // label_fname
            // 
            this.label_fname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_fname.AutoSize = true;
            this.label_fname.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold);
            this.label_fname.ForeColor = System.Drawing.SystemColors.Info;
            this.label_fname.Location = new System.Drawing.Point(654, 11);
            this.label_fname.Name = "label_fname";
            this.label_fname.Size = new System.Drawing.Size(101, 33);
            this.label_fname.TabIndex = 1;
            this.label_fname.Text = "label3";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(217, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 33);
            this.label4.TabIndex = 2;
            this.label4.Text = "Last Name";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(190, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(223, 33);
            this.label5.TabIndex = 3;
            this.label5.Text = "Phone Number";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(252, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 33);
            this.label6.TabIndex = 4;
            this.label6.Text = "Email";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(238, 231);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 33);
            this.label7.TabIndex = 5;
            this.label7.Text = "Address";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(248, 286);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 33);
            this.label8.TabIndex = 6;
            this.label8.Text = "Salary";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(223, 341);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(157, 33);
            this.label9.TabIndex = 7;
            this.label9.Text = "Username";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(227, 396);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(148, 33);
            this.label10.TabIndex = 8;
            this.label10.Text = "Password";
            // 
            // label_lname
            // 
            this.label_lname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_lname.AutoSize = true;
            this.label_lname.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold);
            this.label_lname.ForeColor = System.Drawing.SystemColors.Info;
            this.label_lname.Location = new System.Drawing.Point(645, 66);
            this.label_lname.Name = "label_lname";
            this.label_lname.Size = new System.Drawing.Size(118, 33);
            this.label_lname.TabIndex = 9;
            this.label_lname.Text = "label11";
            // 
            // label_phone
            // 
            this.label_phone.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_phone.AutoSize = true;
            this.label_phone.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold);
            this.label_phone.ForeColor = System.Drawing.SystemColors.Info;
            this.label_phone.Location = new System.Drawing.Point(645, 121);
            this.label_phone.Name = "label_phone";
            this.label_phone.Size = new System.Drawing.Size(118, 33);
            this.label_phone.TabIndex = 10;
            this.label_phone.Text = "label12";
            // 
            // label_email
            // 
            this.label_email.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_email.AutoSize = true;
            this.label_email.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold);
            this.label_email.ForeColor = System.Drawing.SystemColors.Info;
            this.label_email.Location = new System.Drawing.Point(645, 176);
            this.label_email.Name = "label_email";
            this.label_email.Size = new System.Drawing.Size(118, 33);
            this.label_email.TabIndex = 11;
            this.label_email.Text = "label13";
            // 
            // label_salary
            // 
            this.label_salary.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_salary.AutoSize = true;
            this.label_salary.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold);
            this.label_salary.ForeColor = System.Drawing.SystemColors.Info;
            this.label_salary.Location = new System.Drawing.Point(645, 286);
            this.label_salary.Name = "label_salary";
            this.label_salary.Size = new System.Drawing.Size(118, 33);
            this.label_salary.TabIndex = 12;
            this.label_salary.Text = "label14";
            // 
            // label_user
            // 
            this.label_user.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_user.AutoSize = true;
            this.label_user.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold);
            this.label_user.ForeColor = System.Drawing.SystemColors.Info;
            this.label_user.Location = new System.Drawing.Point(645, 341);
            this.label_user.Name = "label_user";
            this.label_user.Size = new System.Drawing.Size(118, 33);
            this.label_user.TabIndex = 13;
            this.label_user.Text = "label15";
            // 
            // label_pass
            // 
            this.label_pass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_pass.AutoSize = true;
            this.label_pass.Font = new System.Drawing.Font("Century", 16.2F, System.Drawing.FontStyle.Bold);
            this.label_pass.ForeColor = System.Drawing.SystemColors.Info;
            this.label_pass.Location = new System.Drawing.Point(645, 396);
            this.label_pass.Name = "label_pass";
            this.label_pass.Size = new System.Drawing.Size(118, 33);
            this.label_pass.TabIndex = 14;
            this.label_pass.Text = "label16";
            // 
            // richTextBox_address
            // 
            this.richTextBox_address.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.richTextBox_address.BackColor = System.Drawing.Color.LightSlateGray;
            this.richTextBox_address.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox_address.Font = new System.Drawing.Font("Century", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_address.ForeColor = System.Drawing.SystemColors.Info;
            this.richTextBox_address.Location = new System.Drawing.Point(545, 223);
            this.richTextBox_address.Name = "richTextBox_address";
            this.richTextBox_address.ReadOnly = true;
            this.richTextBox_address.Size = new System.Drawing.Size(319, 49);
            this.richTextBox_address.TabIndex = 15;
            this.richTextBox_address.Text = "";
            // 
            // Profile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Profile";
            this.Size = new System.Drawing.Size(1014, 524);
            this.Load += new System.EventHandler(this.Profile_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_fname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label_lname;
        private System.Windows.Forms.Label label_phone;
        private System.Windows.Forms.Label label_email;
        private System.Windows.Forms.Label label_salary;
        private System.Windows.Forms.Label label_user;
        private System.Windows.Forms.Label label_pass;
        private System.Windows.Forms.RichTextBox richTextBox_address;
    }
}
