﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacy.Classes.BL
{
    public class SaleReports
    {
        public string Name { get; set; }
        public int UnitRetailPrice { get; set; }
        public int QuantitySold { get; set; }
    }
}
