﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacy.Classes.BL
{
    public  class AdminRequestReport
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string email { get; set; }
        public int salary { get; set; }
        public string Address { get; set; }
        public int Phone { get; set; }
    }
}
