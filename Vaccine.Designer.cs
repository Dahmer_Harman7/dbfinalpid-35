﻿
namespace Pharmacy
{
    partial class Vaccine
    {
        
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_Name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox_descrption = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_stock = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_costPrice = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_retailPrice = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_rackNo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_rowinRack = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox_Route = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox_disease = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_doses = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBox_unit = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox_unitQuantity = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBox_manufecturer = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dateTimePicker_rxpiry = new System.Windows.Forms.DateTimePicker();
            this.btn_add = new Guna.UI2.WinForms.Guna2Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel1.Controls.Add(this.label2);
            this.flowLayoutPanel1.Controls.Add(this.textBox_Name);
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.richTextBox_descrption);
            this.flowLayoutPanel1.Controls.Add(this.label3);
            this.flowLayoutPanel1.Controls.Add(this.textBox_stock);
            this.flowLayoutPanel1.Controls.Add(this.label4);
            this.flowLayoutPanel1.Controls.Add(this.textBox_costPrice);
            this.flowLayoutPanel1.Controls.Add(this.label5);
            this.flowLayoutPanel1.Controls.Add(this.textBox_retailPrice);
            this.flowLayoutPanel1.Controls.Add(this.label6);
            this.flowLayoutPanel1.Controls.Add(this.textBox_rackNo);
            this.flowLayoutPanel1.Controls.Add(this.label7);
            this.flowLayoutPanel1.Controls.Add(this.textBox_rowinRack);
            this.flowLayoutPanel1.Controls.Add(this.label8);
            this.flowLayoutPanel1.Controls.Add(this.comboBox_Route);
            this.flowLayoutPanel1.Controls.Add(this.label9);
            this.flowLayoutPanel1.Controls.Add(this.textBox6);
            this.flowLayoutPanel1.Controls.Add(this.label10);
            this.flowLayoutPanel1.Controls.Add(this.textBox_disease);
            this.flowLayoutPanel1.Controls.Add(this.label11);
            this.flowLayoutPanel1.Controls.Add(this.textBox_doses);
            this.flowLayoutPanel1.Controls.Add(this.label12);
            this.flowLayoutPanel1.Controls.Add(this.comboBox_unit);
            this.flowLayoutPanel1.Controls.Add(this.label13);
            this.flowLayoutPanel1.Controls.Add(this.textBox_unitQuantity);
            this.flowLayoutPanel1.Controls.Add(this.label14);
            this.flowLayoutPanel1.Controls.Add(this.comboBox_manufecturer);
            this.flowLayoutPanel1.Controls.Add(this.label15);
            this.flowLayoutPanel1.Controls.Add(this.dateTimePicker_rxpiry);
            this.flowLayoutPanel1.Controls.Add(this.btn_add);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(390, 482);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Info;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 49);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // textBox_Name
            // 
            this.textBox_Name.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Name.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Name.Location = new System.Drawing.Point(3, 52);
            this.textBox_Name.Name = "textBox_Name";
            this.textBox_Name.Size = new System.Drawing.Size(353, 44);
            this.textBox_Name.TabIndex = 2;
            this.textBox_Name.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_Name_KeyPress);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Info;
            this.label1.Location = new System.Drawing.Point(3, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(258, 49);
            this.label1.TabIndex = 3;
            this.label1.Text = "Description";
            // 
            // richTextBox_descrption
            // 
            this.richTextBox_descrption.Font = new System.Drawing.Font("Noto Serif", 16.2F);
            this.richTextBox_descrption.Location = new System.Drawing.Point(3, 151);
            this.richTextBox_descrption.Name = "richTextBox_descrption";
            this.richTextBox_descrption.Size = new System.Drawing.Size(353, 154);
            this.richTextBox_descrption.TabIndex = 12;
            this.richTextBox_descrption.Text = "";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Info;
            this.label3.Location = new System.Drawing.Point(3, 308);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(329, 49);
            this.label3.TabIndex = 13;
            this.label3.Text = "Stock On Hand";
            // 
            // textBox_stock
            // 
            this.textBox_stock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_stock.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_stock.Location = new System.Drawing.Point(3, 360);
            this.textBox_stock.Name = "textBox_stock";
            this.textBox_stock.Size = new System.Drawing.Size(353, 44);
            this.textBox_stock.TabIndex = 14;
            this.textBox_stock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_stock_KeyPress);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Info;
            this.label4.Location = new System.Drawing.Point(3, 407);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(328, 49);
            this.label4.TabIndex = 15;
            this.label4.Text = "Unit Cost Price";
            // 
            // textBox_costPrice
            // 
            this.textBox_costPrice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_costPrice.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_costPrice.Location = new System.Drawing.Point(3, 459);
            this.textBox_costPrice.Name = "textBox_costPrice";
            this.textBox_costPrice.Size = new System.Drawing.Size(353, 44);
            this.textBox_costPrice.TabIndex = 16;
            this.textBox_costPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_costPrice_KeyPress);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Elephant", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Info;
            this.label5.Location = new System.Drawing.Point(3, 506);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(316, 42);
            this.label5.TabIndex = 17;
            this.label5.Text = "Unit Retail Price";
            // 
            // textBox_retailPrice
            // 
            this.textBox_retailPrice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_retailPrice.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_retailPrice.Location = new System.Drawing.Point(3, 551);
            this.textBox_retailPrice.Name = "textBox_retailPrice";
            this.textBox_retailPrice.Size = new System.Drawing.Size(353, 44);
            this.textBox_retailPrice.TabIndex = 18;
            this.textBox_retailPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_retailPrice_KeyPress);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.Info;
            this.label6.Location = new System.Drawing.Point(3, 598);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(157, 49);
            this.label6.TabIndex = 19;
            this.label6.Text = "Rack#";
            // 
            // textBox_rackNo
            // 
            this.textBox_rackNo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_rackNo.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_rackNo.Location = new System.Drawing.Point(3, 650);
            this.textBox_rackNo.Name = "textBox_rackNo";
            this.textBox_rackNo.Size = new System.Drawing.Size(353, 44);
            this.textBox_rackNo.TabIndex = 20;
            this.textBox_rackNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_rackNo_KeyPress);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.Info;
            this.label7.Location = new System.Drawing.Point(3, 697);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(278, 49);
            this.label7.TabIndex = 21;
            this.label7.Text = "Row in Rack";
            // 
            // textBox_rowinRack
            // 
            this.textBox_rowinRack.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_rowinRack.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_rowinRack.Location = new System.Drawing.Point(3, 749);
            this.textBox_rowinRack.Name = "textBox_rowinRack";
            this.textBox_rowinRack.Size = new System.Drawing.Size(353, 44);
            this.textBox_rowinRack.TabIndex = 22;
            this.textBox_rowinRack.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_rowinRack_KeyPress);
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Elephant", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.Info;
            this.label8.Location = new System.Drawing.Point(3, 796);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(327, 37);
            this.label8.TabIndex = 23;
            this.label8.Text = "Adminitration Route";
            // 
            // comboBox_Route
            // 
            this.comboBox_Route.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboBox_Route.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Route.Font = new System.Drawing.Font("Cambria", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_Route.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.comboBox_Route.FormattingEnabled = true;
            this.comboBox_Route.Items.AddRange(new object[] {
            "Vein",
            "Muscle"});
            this.comboBox_Route.Location = new System.Drawing.Point(3, 836);
            this.comboBox_Route.Name = "comboBox_Route";
            this.comboBox_Route.Size = new System.Drawing.Size(353, 40);
            this.comboBox_Route.TabIndex = 55;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.Info;
            this.label9.Location = new System.Drawing.Point(3, 879);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(307, 49);
            this.label9.TabIndex = 56;
            this.label9.Text = "Duration Last";
            // 
            // textBox6
            // 
            this.textBox6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox6.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(3, 931);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(353, 44);
            this.textBox6.TabIndex = 57;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Elephant", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.Info;
            this.label10.Location = new System.Drawing.Point(3, 978);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(311, 38);
            this.label10.TabIndex = 58;
            this.label10.Text = "Disease Prevented";
            // 
            // textBox_disease
            // 
            this.textBox_disease.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_disease.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_disease.Location = new System.Drawing.Point(3, 1019);
            this.textBox_disease.Name = "textBox_disease";
            this.textBox_disease.Size = new System.Drawing.Size(353, 44);
            this.textBox_disease.TabIndex = 59;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Elephant", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.Info;
            this.label11.Location = new System.Drawing.Point(3, 1066);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(332, 37);
            this.label11.TabIndex = 60;
            this.label11.Text = "Recommended Doses";
            // 
            // textBox_doses
            // 
            this.textBox_doses.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_doses.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_doses.Location = new System.Drawing.Point(3, 1106);
            this.textBox_doses.Name = "textBox_doses";
            this.textBox_doses.Size = new System.Drawing.Size(353, 44);
            this.textBox_doses.TabIndex = 61;
            this.textBox_doses.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_doses_KeyPress);
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.Info;
            this.label12.Location = new System.Drawing.Point(3, 1153);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 49);
            this.label12.TabIndex = 62;
            this.label12.Text = "Unit";
            // 
            // comboBox_unit
            // 
            this.comboBox_unit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboBox_unit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_unit.Font = new System.Drawing.Font("Cambria", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_unit.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.comboBox_unit.FormattingEnabled = true;
            this.comboBox_unit.Items.AddRange(new object[] {
            "mg",
            "ml"});
            this.comboBox_unit.Location = new System.Drawing.Point(3, 1205);
            this.comboBox_unit.Name = "comboBox_unit";
            this.comboBox_unit.Size = new System.Drawing.Size(353, 40);
            this.comboBox_unit.TabIndex = 63;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.Info;
            this.label13.Location = new System.Drawing.Point(3, 1248);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(271, 49);
            this.label13.TabIndex = 64;
            this.label13.Text = "Unit Quatity";
            // 
            // textBox_unitQuantity
            // 
            this.textBox_unitQuantity.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_unitQuantity.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_unitQuantity.Location = new System.Drawing.Point(3, 1300);
            this.textBox_unitQuantity.Name = "textBox_unitQuantity";
            this.textBox_unitQuantity.Size = new System.Drawing.Size(353, 44);
            this.textBox_unitQuantity.TabIndex = 65;
            this.textBox_unitQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_unitQuantity_KeyPress);
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.Info;
            this.label14.Location = new System.Drawing.Point(3, 1347);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(299, 49);
            this.label14.TabIndex = 75;
            this.label14.Text = "Manufecturer";
            // 
            // comboBox_manufecturer
            // 
            this.comboBox_manufecturer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboBox_manufecturer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_manufecturer.Font = new System.Drawing.Font("Cambria", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_manufecturer.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.comboBox_manufecturer.FormattingEnabled = true;
            this.comboBox_manufecturer.Location = new System.Drawing.Point(3, 1399);
            this.comboBox_manufecturer.Name = "comboBox_manufecturer";
            this.comboBox_manufecturer.Size = new System.Drawing.Size(353, 40);
            this.comboBox_manufecturer.TabIndex = 76;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.Info;
            this.label15.Location = new System.Drawing.Point(3, 1442);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(264, 49);
            this.label15.TabIndex = 77;
            this.label15.Text = "Expiry Date";
            // 
            // dateTimePicker_rxpiry
            // 
            this.dateTimePicker_rxpiry.CalendarForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dateTimePicker_rxpiry.Font = new System.Drawing.Font("Cambria", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker_rxpiry.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_rxpiry.Location = new System.Drawing.Point(3, 1494);
            this.dateTimePicker_rxpiry.Name = "dateTimePicker_rxpiry";
            this.dateTimePicker_rxpiry.Size = new System.Drawing.Size(353, 46);
            this.dateTimePicker_rxpiry.TabIndex = 78;
            // 
            // btn_add
            // 
            this.btn_add.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_add.Animated = true;
            this.btn_add.BorderRadius = 25;
            this.btn_add.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            this.btn_add.BorderThickness = 3;
            this.btn_add.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_add.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_add.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_add.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_add.FillColor = System.Drawing.Color.DarkKhaki;
            this.btn_add.Font = new System.Drawing.Font("Cambria", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.SystemColors.Info;
            this.btn_add.Location = new System.Drawing.Point(3, 1546);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(353, 72);
            this.btn_add.TabIndex = 79;
            this.btn_add.Text = "Add Vaccine";
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // Vaccine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 482);
            this.Controls.Add(this.flowLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Vaccine";
            this.Text = "Vaccine";
            this.Load += new System.EventHandler(this.Vaccine_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_Name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox_descrption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_stock;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_costPrice;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_retailPrice;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_rackNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_rowinRack;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox_Route;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_disease;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox_doses;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBox_unit;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox_unitQuantity;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBox_manufecturer;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker dateTimePicker_rxpiry;
        private Guna.UI2.WinForms.Guna2Button btn_add;
    }
}