﻿
namespace Pharmacy
{
    partial class EditManufecturer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_Name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_phone1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_phone2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.richTextBox_address1 = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.richTextBox_address2 = new System.Windows.Forms.RichTextBox();
            this.btn_save = new Guna.UI2.WinForms.Guna2Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel1.Controls.Add(this.label2);
            this.flowLayoutPanel1.Controls.Add(this.textBox_Name);
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.textBox_phone1);
            this.flowLayoutPanel1.Controls.Add(this.label3);
            this.flowLayoutPanel1.Controls.Add(this.textBox_phone2);
            this.flowLayoutPanel1.Controls.Add(this.label4);
            this.flowLayoutPanel1.Controls.Add(this.richTextBox_address1);
            this.flowLayoutPanel1.Controls.Add(this.label5);
            this.flowLayoutPanel1.Controls.Add(this.richTextBox_address2);
            this.flowLayoutPanel1.Controls.Add(this.btn_save);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(390, 482);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Info;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 49);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // textBox_Name
            // 
            this.textBox_Name.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Name.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Name.Location = new System.Drawing.Point(3, 52);
            this.textBox_Name.Name = "textBox_Name";
            this.textBox_Name.Size = new System.Drawing.Size(353, 44);
            this.textBox_Name.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Info;
            this.label1.Location = new System.Drawing.Point(3, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 49);
            this.label1.TabIndex = 3;
            this.label1.Text = "Phone 1";
            // 
            // textBox_phone1
            // 
            this.textBox_phone1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_phone1.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_phone1.Location = new System.Drawing.Point(3, 151);
            this.textBox_phone1.Name = "textBox_phone1";
            this.textBox_phone1.Size = new System.Drawing.Size(353, 44);
            this.textBox_phone1.TabIndex = 4;
            this.textBox_phone1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_phone1_KeyPress);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Info;
            this.label3.Location = new System.Drawing.Point(3, 198);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(189, 49);
            this.label3.TabIndex = 5;
            this.label3.Text = "Phone 2";
            // 
            // textBox_phone2
            // 
            this.textBox_phone2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_phone2.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_phone2.Location = new System.Drawing.Point(3, 250);
            this.textBox_phone2.Name = "textBox_phone2";
            this.textBox_phone2.Size = new System.Drawing.Size(353, 44);
            this.textBox_phone2.TabIndex = 6;
            this.textBox_phone2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_phone2_KeyPress);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Info;
            this.label4.Location = new System.Drawing.Point(3, 297);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(211, 49);
            this.label4.TabIndex = 7;
            this.label4.Text = "Address 1";
            // 
            // richTextBox_address1
            // 
            this.richTextBox_address1.Font = new System.Drawing.Font("Noto Serif", 16.2F);
            this.richTextBox_address1.Location = new System.Drawing.Point(3, 349);
            this.richTextBox_address1.Name = "richTextBox_address1";
            this.richTextBox_address1.Size = new System.Drawing.Size(353, 154);
            this.richTextBox_address1.TabIndex = 11;
            this.richTextBox_address1.Text = "";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Info;
            this.label5.Location = new System.Drawing.Point(3, 506);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(219, 49);
            this.label5.TabIndex = 12;
            this.label5.Text = "Address 2";
            // 
            // richTextBox_address2
            // 
            this.richTextBox_address2.Font = new System.Drawing.Font("Noto Serif", 16.2F);
            this.richTextBox_address2.Location = new System.Drawing.Point(3, 558);
            this.richTextBox_address2.Name = "richTextBox_address2";
            this.richTextBox_address2.Size = new System.Drawing.Size(353, 154);
            this.richTextBox_address2.TabIndex = 13;
            this.richTextBox_address2.Text = "";
            // 
            // btn_save
            // 
            this.btn_save.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_save.Animated = true;
            this.btn_save.BorderRadius = 25;
            this.btn_save.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            this.btn_save.BorderThickness = 3;
            this.btn_save.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_save.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_save.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_save.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_save.FillColor = System.Drawing.Color.DarkKhaki;
            this.btn_save.Font = new System.Drawing.Font("Cambria", 28.2F, System.Drawing.FontStyle.Bold);
            this.btn_save.ForeColor = System.Drawing.SystemColors.Info;
            this.btn_save.Location = new System.Drawing.Point(3, 718);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(353, 72);
            this.btn_save.TabIndex = 52;
            this.btn_save.Text = "Save";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // EditManufecturer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 482);
            this.Controls.Add(this.flowLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EditManufecturer";
            this.Text = "Editor";
            this.Load += new System.EventHandler(this.EditManufecturer_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_Name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_phone1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_phone2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox richTextBox_address1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox richTextBox_address2;
        private Guna.UI2.WinForms.Guna2Button btn_save;
    }
}