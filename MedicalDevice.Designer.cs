﻿
namespace Pharmacy
{
    partial class MedicalDevice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_Name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox_description = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_stock = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_unitCost = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_retailPrice = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_rackNo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_RowinRack = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox_WF = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox_WD = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_organ = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBox_Manufecturer = new System.Windows.Forms.ComboBox();
            this.btn_add = new Guna.UI2.WinForms.Guna2Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel1.Controls.Add(this.label2);
            this.flowLayoutPanel1.Controls.Add(this.textBox_Name);
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.richTextBox_description);
            this.flowLayoutPanel1.Controls.Add(this.label3);
            this.flowLayoutPanel1.Controls.Add(this.textBox_stock);
            this.flowLayoutPanel1.Controls.Add(this.label4);
            this.flowLayoutPanel1.Controls.Add(this.textBox_unitCost);
            this.flowLayoutPanel1.Controls.Add(this.label5);
            this.flowLayoutPanel1.Controls.Add(this.textBox_retailPrice);
            this.flowLayoutPanel1.Controls.Add(this.label6);
            this.flowLayoutPanel1.Controls.Add(this.textBox_rackNo);
            this.flowLayoutPanel1.Controls.Add(this.label7);
            this.flowLayoutPanel1.Controls.Add(this.textBox_RowinRack);
            this.flowLayoutPanel1.Controls.Add(this.label11);
            this.flowLayoutPanel1.Controls.Add(this.comboBox_WF);
            this.flowLayoutPanel1.Controls.Add(this.label8);
            this.flowLayoutPanel1.Controls.Add(this.textBox_WD);
            this.flowLayoutPanel1.Controls.Add(this.label9);
            this.flowLayoutPanel1.Controls.Add(this.textBox_organ);
            this.flowLayoutPanel1.Controls.Add(this.label14);
            this.flowLayoutPanel1.Controls.Add(this.comboBox_Manufecturer);
            this.flowLayoutPanel1.Controls.Add(this.btn_add);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(390, 482);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Info;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 49);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // textBox_Name
            // 
            this.textBox_Name.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Name.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Name.Location = new System.Drawing.Point(3, 52);
            this.textBox_Name.Name = "textBox_Name";
            this.textBox_Name.Size = new System.Drawing.Size(353, 44);
            this.textBox_Name.TabIndex = 2;
            this.textBox_Name.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_Name_KeyPress);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Info;
            this.label1.Location = new System.Drawing.Point(3, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(258, 49);
            this.label1.TabIndex = 3;
            this.label1.Text = "Description";
            // 
            // richTextBox_description
            // 
            this.richTextBox_description.Font = new System.Drawing.Font("Noto Serif", 16.2F);
            this.richTextBox_description.Location = new System.Drawing.Point(3, 151);
            this.richTextBox_description.Name = "richTextBox_description";
            this.richTextBox_description.Size = new System.Drawing.Size(353, 154);
            this.richTextBox_description.TabIndex = 12;
            this.richTextBox_description.Text = "";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Info;
            this.label3.Location = new System.Drawing.Point(3, 308);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(329, 49);
            this.label3.TabIndex = 13;
            this.label3.Text = "Stock On Hand";
            // 
            // textBox_stock
            // 
            this.textBox_stock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_stock.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_stock.Location = new System.Drawing.Point(3, 360);
            this.textBox_stock.Name = "textBox_stock";
            this.textBox_stock.Size = new System.Drawing.Size(353, 44);
            this.textBox_stock.TabIndex = 14;
            this.textBox_stock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_stock_KeyPress);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Info;
            this.label4.Location = new System.Drawing.Point(3, 407);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(328, 49);
            this.label4.TabIndex = 15;
            this.label4.Text = "Unit Cost Price";
            // 
            // textBox_unitCost
            // 
            this.textBox_unitCost.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_unitCost.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_unitCost.Location = new System.Drawing.Point(3, 459);
            this.textBox_unitCost.Name = "textBox_unitCost";
            this.textBox_unitCost.Size = new System.Drawing.Size(353, 44);
            this.textBox_unitCost.TabIndex = 16;
            this.textBox_unitCost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_unitCost_KeyPress);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Elephant", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Info;
            this.label5.Location = new System.Drawing.Point(3, 506);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(316, 42);
            this.label5.TabIndex = 17;
            this.label5.Text = "Unit Retail Price";
            // 
            // textBox_retailPrice
            // 
            this.textBox_retailPrice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_retailPrice.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_retailPrice.Location = new System.Drawing.Point(3, 551);
            this.textBox_retailPrice.Name = "textBox_retailPrice";
            this.textBox_retailPrice.Size = new System.Drawing.Size(353, 44);
            this.textBox_retailPrice.TabIndex = 18;
            this.textBox_retailPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_retailPrice_KeyPress);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.Info;
            this.label6.Location = new System.Drawing.Point(3, 598);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(157, 49);
            this.label6.TabIndex = 19;
            this.label6.Text = "Rack#";
            // 
            // textBox_rackNo
            // 
            this.textBox_rackNo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_rackNo.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_rackNo.Location = new System.Drawing.Point(3, 650);
            this.textBox_rackNo.Name = "textBox_rackNo";
            this.textBox_rackNo.Size = new System.Drawing.Size(353, 44);
            this.textBox_rackNo.TabIndex = 20;
            this.textBox_rackNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_rackNo_KeyPress);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.Info;
            this.label7.Location = new System.Drawing.Point(3, 697);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(278, 49);
            this.label7.TabIndex = 21;
            this.label7.Text = "Row in Rack";
            // 
            // textBox_RowinRack
            // 
            this.textBox_RowinRack.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_RowinRack.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_RowinRack.Location = new System.Drawing.Point(3, 749);
            this.textBox_RowinRack.Name = "textBox_RowinRack";
            this.textBox_RowinRack.Size = new System.Drawing.Size(353, 44);
            this.textBox_RowinRack.TabIndex = 22;
            this.textBox_RowinRack.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_RowinRack_KeyPress_1);
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Elephant", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.Info;
            this.label11.Location = new System.Drawing.Point(3, 796);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(286, 38);
            this.label11.TabIndex = 77;
            this.label11.Text = "Warrenty Figure";
            // 
            // comboBox_WF
            // 
            this.comboBox_WF.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboBox_WF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_WF.Font = new System.Drawing.Font("Cambria", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_WF.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.comboBox_WF.FormattingEnabled = true;
            this.comboBox_WF.Items.AddRange(new object[] {
            "Year",
            "Month",
            "Day"});
            this.comboBox_WF.Location = new System.Drawing.Point(3, 837);
            this.comboBox_WF.Name = "comboBox_WF";
            this.comboBox_WF.Size = new System.Drawing.Size(353, 40);
            this.comboBox_WF.TabIndex = 78;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Elephant", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.Info;
            this.label8.Location = new System.Drawing.Point(3, 880);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(326, 38);
            this.label8.TabIndex = 79;
            this.label8.Text = "Warrenty Duration";
            // 
            // textBox_WD
            // 
            this.textBox_WD.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_WD.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_WD.Location = new System.Drawing.Point(3, 921);
            this.textBox_WD.Name = "textBox_WD";
            this.textBox_WD.Size = new System.Drawing.Size(353, 44);
            this.textBox_WD.TabIndex = 80;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Elephant", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.Info;
            this.label9.Location = new System.Drawing.Point(3, 968);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(292, 38);
            this.label9.TabIndex = 81;
            this.label9.Text = "Organ Diagnosed";
            // 
            // textBox_organ
            // 
            this.textBox_organ.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_organ.Font = new System.Drawing.Font("Noto Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_organ.Location = new System.Drawing.Point(3, 1009);
            this.textBox_organ.Name = "textBox_organ";
            this.textBox_organ.Size = new System.Drawing.Size(353, 44);
            this.textBox_organ.TabIndex = 82;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Elephant", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.Info;
            this.label14.Location = new System.Drawing.Point(3, 1056);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(299, 49);
            this.label14.TabIndex = 83;
            this.label14.Text = "Manufecturer";
            // 
            // comboBox_Manufecturer
            // 
            this.comboBox_Manufecturer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboBox_Manufecturer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Manufecturer.Font = new System.Drawing.Font("Cambria", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_Manufecturer.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.comboBox_Manufecturer.FormattingEnabled = true;
            this.comboBox_Manufecturer.Location = new System.Drawing.Point(3, 1108);
            this.comboBox_Manufecturer.Name = "comboBox_Manufecturer";
            this.comboBox_Manufecturer.Size = new System.Drawing.Size(353, 40);
            this.comboBox_Manufecturer.TabIndex = 84;
            // 
            // btn_add
            // 
            this.btn_add.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_add.Animated = true;
            this.btn_add.BorderRadius = 25;
            this.btn_add.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            this.btn_add.BorderThickness = 3;
            this.btn_add.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_add.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_add.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_add.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_add.FillColor = System.Drawing.Color.DarkKhaki;
            this.btn_add.Font = new System.Drawing.Font("Cambria", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.SystemColors.Info;
            this.btn_add.Location = new System.Drawing.Point(3, 1154);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(353, 72);
            this.btn_add.TabIndex = 85;
            this.btn_add.Text = "Add Device";
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // MedicalDevice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 482);
            this.Controls.Add(this.flowLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MedicalDevice";
            this.Text = "MedicalDevice";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_Name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox_description;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_stock;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_unitCost;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_retailPrice;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_rackNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_RowinRack;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox_WF;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_WD;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_organ;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBox_Manufecturer;
        private Guna.UI2.WinForms.Guna2Button btn_add;
    }
}