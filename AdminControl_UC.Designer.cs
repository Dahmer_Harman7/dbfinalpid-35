﻿
namespace Pharmacy
{
    partial class AdminControl_UC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminControl_UC));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_reports = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Sale = new Guna.UI2.WinForms.Guna2Button();
            this.btn_adminReq = new Guna.UI2.WinForms.Guna2Button();
            this.btn_prod = new Guna.UI2.WinForms.Guna2Button();
            this.btn_manufecturer = new Guna.UI2.WinForms.Guna2Button();
            this.btn_profile = new Guna.UI2.WinForms.Guna2Button();
            this.panel_ucContainer = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.adminRequest1 = new Pharmacy.AdminRequest();
            this.product1 = new Pharmacy.Product();
            this.manufecturer1 = new Pharmacy.Manufecturer();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel_ucContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 351F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel_ucContainer, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1503, 668);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 4);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(345, 660);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.btn_reports, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.btn_Sale, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.btn_adminReq, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.btn_prod, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btn_manufecturer, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_profile, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(341, 656);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // btn_reports
            // 
            this.btn_reports.Animated = true;
            this.btn_reports.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_reports.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_reports.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_reports.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_reports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_reports.FillColor = System.Drawing.Color.Transparent;
            this.btn_reports.Font = new System.Drawing.Font("Castellar", 16.2F, System.Drawing.FontStyle.Bold);
            this.btn_reports.ForeColor = System.Drawing.SystemColors.Info;
            this.btn_reports.Location = new System.Drawing.Point(3, 549);
            this.btn_reports.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_reports.Name = "btn_reports";
            this.btn_reports.Size = new System.Drawing.Size(335, 103);
            this.btn_reports.TabIndex = 6;
            this.btn_reports.Text = "Reports";
            this.btn_reports.Click += new System.EventHandler(this.btn_reports_Click);
            // 
            // btn_Sale
            // 
            this.btn_Sale.Animated = true;
            this.btn_Sale.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Sale.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Sale.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Sale.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Sale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Sale.FillColor = System.Drawing.Color.Transparent;
            this.btn_Sale.Font = new System.Drawing.Font("Castellar", 16.2F, System.Drawing.FontStyle.Bold);
            this.btn_Sale.ForeColor = System.Drawing.SystemColors.Info;
            this.btn_Sale.Location = new System.Drawing.Point(3, 440);
            this.btn_Sale.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_Sale.Name = "btn_Sale";
            this.btn_Sale.Size = new System.Drawing.Size(335, 101);
            this.btn_Sale.TabIndex = 5;
            this.btn_Sale.Text = "Sale";
            this.btn_Sale.Click += new System.EventHandler(this.btn_Sale_Click);
            // 
            // btn_adminReq
            // 
            this.btn_adminReq.Animated = true;
            this.btn_adminReq.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_adminReq.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_adminReq.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_adminReq.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_adminReq.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_adminReq.FillColor = System.Drawing.Color.Transparent;
            this.btn_adminReq.Font = new System.Drawing.Font("Castellar", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_adminReq.ForeColor = System.Drawing.SystemColors.Info;
            this.btn_adminReq.Location = new System.Drawing.Point(3, 331);
            this.btn_adminReq.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_adminReq.Name = "btn_adminReq";
            this.btn_adminReq.Size = new System.Drawing.Size(335, 101);
            this.btn_adminReq.TabIndex = 4;
            this.btn_adminReq.Text = "Admin Request";
            this.btn_adminReq.Click += new System.EventHandler(this.btn_adminReq_Click);
            // 
            // btn_prod
            // 
            this.btn_prod.Animated = true;
            this.btn_prod.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_prod.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_prod.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_prod.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_prod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_prod.FillColor = System.Drawing.Color.Transparent;
            this.btn_prod.Font = new System.Drawing.Font("Castellar", 16.2F, System.Drawing.FontStyle.Bold);
            this.btn_prod.ForeColor = System.Drawing.SystemColors.Info;
            this.btn_prod.Location = new System.Drawing.Point(3, 222);
            this.btn_prod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_prod.Name = "btn_prod";
            this.btn_prod.Size = new System.Drawing.Size(335, 101);
            this.btn_prod.TabIndex = 3;
            this.btn_prod.Text = "Product";
            this.btn_prod.Click += new System.EventHandler(this.btn_prod_Click);
            // 
            // btn_manufecturer
            // 
            this.btn_manufecturer.Animated = true;
            this.btn_manufecturer.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_manufecturer.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_manufecturer.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_manufecturer.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_manufecturer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_manufecturer.FillColor = System.Drawing.Color.Transparent;
            this.btn_manufecturer.Font = new System.Drawing.Font("Castellar", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_manufecturer.ForeColor = System.Drawing.SystemColors.Info;
            this.btn_manufecturer.Location = new System.Drawing.Point(3, 113);
            this.btn_manufecturer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_manufecturer.Name = "btn_manufecturer";
            this.btn_manufecturer.Size = new System.Drawing.Size(335, 101);
            this.btn_manufecturer.TabIndex = 2;
            this.btn_manufecturer.Text = "Manufecturer";
            this.btn_manufecturer.Click += new System.EventHandler(this.btn_manufecturer_Click);
            // 
            // btn_profile
            // 
            this.btn_profile.Animated = true;
            this.btn_profile.BackColor = System.Drawing.Color.Transparent;
            this.btn_profile.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_profile.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_profile.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_profile.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_profile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_profile.FillColor = System.Drawing.Color.Transparent;
            this.btn_profile.Font = new System.Drawing.Font("Castellar", 16.2F, System.Drawing.FontStyle.Bold);
            this.btn_profile.ForeColor = System.Drawing.SystemColors.Info;
            this.btn_profile.Location = new System.Drawing.Point(3, 4);
            this.btn_profile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_profile.Name = "btn_profile";
            this.btn_profile.Size = new System.Drawing.Size(335, 101);
            this.btn_profile.TabIndex = 1;
            this.btn_profile.Text = "Profile";
            this.btn_profile.Click += new System.EventHandler(this.btn_profile_Click);
            // 
            // panel_ucContainer
            // 
            this.panel_ucContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel_ucContainer.Controls.Add(this.adminRequest1);
            this.panel_ucContainer.Controls.Add(this.product1);
            this.panel_ucContainer.Controls.Add(this.manufecturer1);
            this.panel_ucContainer.Controls.Add(this.pictureBox1);
            this.panel_ucContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_ucContainer.Location = new System.Drawing.Point(354, 4);
            this.panel_ucContainer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_ucContainer.Name = "panel_ucContainer";
            this.panel_ucContainer.Size = new System.Drawing.Size(1146, 660);
            this.panel_ucContainer.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1142, 656);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // adminRequest1
            // 
            this.adminRequest1.BackColor = System.Drawing.Color.LightSlateGray;
            this.adminRequest1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.adminRequest1.Location = new System.Drawing.Point(0, 0);
            this.adminRequest1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.adminRequest1.Name = "adminRequest1";
            this.adminRequest1.Size = new System.Drawing.Size(1142, 656);
            this.adminRequest1.TabIndex = 4;
            this.adminRequest1.Load += new System.EventHandler(this.adminRequest1_Load);
            // 
            // product1
            // 
            this.product1.BackColor = System.Drawing.Color.LightSlateGray;
            this.product1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.product1.Location = new System.Drawing.Point(0, 0);
            this.product1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.product1.Name = "product1";
            this.product1.Size = new System.Drawing.Size(1142, 656);
            this.product1.TabIndex = 3;
            // 
            // manufecturer1
            // 
            this.manufecturer1.BackColor = System.Drawing.Color.LightSlateGray;
            this.manufecturer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manufecturer1.Location = new System.Drawing.Point(0, 0);
            this.manufecturer1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.manufecturer1.Name = "manufecturer1";
            this.manufecturer1.Size = new System.Drawing.Size(1142, 656);
            this.manufecturer1.TabIndex = 2;
            // 
            // AdminControl_UC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AdminControl_UC";
            this.Size = new System.Drawing.Size(1503, 668);
            this.Load += new System.EventHandler(this.AdminControl_UC_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel_ucContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Guna.UI2.WinForms.Guna2Button btn_profile;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Guna.UI2.WinForms.Guna2Button btn_Sale;
        private Guna.UI2.WinForms.Guna2Button btn_adminReq;
        private Guna.UI2.WinForms.Guna2Button btn_prod;
        private Guna.UI2.WinForms.Guna2Button btn_manufecturer;
        private System.Windows.Forms.Panel panel_ucContainer;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Profile profile1;
        private Sale sale1;
        private AdminRequest adminRequest1;
        private Product product1;
        private Manufecturer manufecturer1;
        private Guna.UI2.WinForms.Guna2Button btn_reports;
    }
}
